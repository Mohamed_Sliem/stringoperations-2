package org.eghagha.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
	OperationWcParameterizedTest.class,
	OperationGrepParameterizedTest.class,
	OperationFreqParameterizedTest.class,
	OperationSuperClassParameterizedTest.class,
	OperationSuperClassExceptionTest.class
})

public class JunitTestSuite {   
}  	
