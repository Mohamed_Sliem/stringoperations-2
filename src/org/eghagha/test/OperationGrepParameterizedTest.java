package org.eghagha.test;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.eghagha.csce.Context;
import org.eghagha.csce.OperationFreq;
import org.eghagha.csce.OperationGrep;
import org.eghagha.csce.OperationWc;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class OperationGrepParameterizedTest {

	OperationGrep og;
	
    // fields used together with @Parameter must be public
    @Parameter(0)
    public String s1;
    @Parameter(1)
    public String s2;    
    @Parameter(2)
    public String result;
    @Parameter(3)
    public boolean b1;
    
    
    @Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][] { { "j", "10by10.txt", "1", true }, { "j", "5by5.txt", "2",true }, { "1", "1by1.txt", "1",true },{ "", "1by1.txt", "",true } };
        return Arrays.asList(data);
    }
	
	@Test
	public void testGrep() {
		
      System.out.println("Grep for " + s1 + " in " + s2);
      og = new OperationGrep(s1, s2);
      og.doOperation();
      assertEquals("Result", result, og.toString());
      assertEquals("Result", true , og.isThereLine());   
      


	}

}
