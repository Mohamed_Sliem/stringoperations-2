package org.eghagha.test;
/*
 * 
 * This class runs all 4 Test files
 */

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class OperationTestRunner {
  public static void main(String[] args) {
	

      Result result = JUnitCore.runClasses(JunitTestSuite.class);

      for (Failure failure : result.getFailures()) {
         System.out.println(failure.toString());
      }
		
  }
}