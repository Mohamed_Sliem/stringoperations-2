package org.eghagha.csce;


public class Context {
	
   private Strategy strategy;

   public Context(){
   }
   
   public void setStrategy(Strategy strategy){
	      this.strategy = strategy;	   
   }

   public void executeStrategy(){
       strategy.doOperation();
   }
   
   public String toString(){
	   return strategy.toString();
   }
   
}
