package org.eghagha.csce;

import java.util.StringTokenizer;

public class OperationWc extends OperationSuperClass implements Strategy {

	private int numberOfLines_counter, word_counter, character_counter = 0;

	public OperationWc(String address) {
		super(address);
	}

	@Override
	public void doOperation() {

		try {	

			// use a StringTokeniner to parse the words, count them
			StringTokenizer tokenizer = new StringTokenizer(allWords);
			setWord_counter(tokenizer.countTokens());

			// convert the whole string to an array of characters, count the characters
			char[] charArray = allWords.trim().toCharArray(); //error in white space.
			setCharacter_counter(charArray.length);
			setNumberOfLines_counter(lineArray.size());
			
			// the number of lines in inherited from the super class
			System.out.println("     line_count : " + getNumberOfLines_counter());
			System.out.println("     word count : " + getWord_counter());
			System.out.println("character count : " + getCharacter_counter());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.toString());
		}

	}
	 public void doOperation(String input){
		 linec(input);
		 wordc(input);
		 charc(input);
		 
		 
	 }
	 public void linec(String d) {
		 try {
			   if(d != null) {
				   numberOfLines_counter++;
			   }
		 }
		 catch(Exception e) {
			   e.printStackTrace();
		   }
	 }
	 public void wordc(String d) {
		 try {
			   if(d != null) {
				   String[] words = d.split(" ");
				   word_counter += words.length;
			   }
		 }
		 catch(Exception e) {
			   e.printStackTrace();
		   }
	 }
	 public void charc(String d) {
		 try {
			   if(d != null) {
				   String[] words = d.split(" ");
				   for (String indword :words) {
					   character_counter += indword.length();
				   }
			   }
		 }
		 catch(Exception e) {
			   e.printStackTrace();
		   }
	 }

	private int getCharacter_counter() {
		return character_counter;
	}

	private void setCharacter_counter(int character_counter) {
		this.character_counter = character_counter;
	}

	private int getWord_counter() {
		return word_counter;
	}

	private void setWord_counter(int word_counter) {
		this.word_counter = word_counter;
	}

	public String toString() {
		return getCharacter_counter() + "_" + getWord_counter() + "_" + numberOfLines_counter;

	}

	private int getNumberOfLines_counter() {
		return numberOfLines_counter;
	}

	private void setNumberOfLines_counter(int numberOfLines_counter) {
		this.numberOfLines_counter = numberOfLines_counter;
	}
	
	public boolean isThereAllWords(){
		return(allWords.length() > 0);
	}
}
