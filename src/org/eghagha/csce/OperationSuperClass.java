package org.eghagha.csce;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
/*
 * 
 * This super class 
 */
public class OperationSuperClass {
    Logger logger = Logger.getLogger("MyLog");  
    FileHandler fh;  
	public String address = "";
	public String allWords = "";
	public ArrayList<String> lineArray = null;
	public String [] wordsArray = null;

	public OperationSuperClass(String address) {
        try {
			fh = new FileHandler("MyLogFile.log");
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
        logger.addHandler(fh);
		this.address = address;
		lineArray = new ArrayList<String>();
		readFile(this.address);
	}	

	public void readFile(String address) {

		BufferedReader in;

		String line;

		try {

			in = new BufferedReader(new FileReader(address));
/*
 * one loop for reading from a file, 
 * populate 2 data structures lineArray[] and wordsArray[]
 * 
 * both data structures will be inherited by sub classes
 */
			int x = 0;
			while ((line = in.readLine()) != null & line != "") {
				lineArray.add(line);
				setAllWords(getAllWords() + line + " ");
				System.out.println(x++);

			}
			String[] splited = getAllWords().split(" ");
			wordsArray = splited;

			in.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
	        logger.info(e.toString());  
		} catch (IOException e) {
			// TODO Auto-generated catch block
	        logger.info(e.toString());  
		}

	}

	public String getAllWords() {
		return allWords;
	}

	public void setAllWords(String allWords) {
		this.allWords = allWords;
	}
	
	public boolean isThereLineArray(){
		return (!(lineArray == null));
	}
	
	public boolean isThereWordsArray(){
		return (!(wordsArray == null));
	}
	public String toString(){
		return (lineArray.size() + "_" + wordsArray.length);
	}
	
}
